#include <iostream>
#include <cstdio>
#include <time.h>
#include <vector>

using namespace std;

#define REP(i, n) for(int (i)=0; (i)<(n); (i)++)
#define REP1(i, n) for(int (i)=1; (i)<=(n); (i)++)
#define MAXN 42

const int N = 9, n = 3;

void OK() { printf("OK\n"); }
void Init();
void PRINT();

int filled;
int grid[MAXN][MAXN]; //keeps current sudoku grid
bool row[MAXN][MAXN], col[MAXN][MAXN], blck[MAXN][MAXN];
//row[i][j] = true if i-th row has j in it
//same for col
//blck similar. blck is 3x3 block as follows:
// 0 1 2
// 3 4 5
// 6 7 8

//gives the index of blck which a cell is in
int f(int r, int c) {
	return n*(r/n)+c/n;
}

void InsertCell(int r, int c, int val) {
	grid[r][c] = val;
	row[r][val] = col[c][val] = blck[f(r,c)][val] = true;
}
void RemoveCell(int r, int c, int val) {
	grid[r][c] = 0;
	row[r][val] = col[c][val] = blck[f(r,c)][val] = false;
}
bool CanPut(int r, int c, int val) {
	return !row[r][val] && !col[c][val] && !blck[f(r,c)][val];
}

//check if there's exactly one possible number to put into this cell
int CheckCell(int r, int c) {
	int count=0, ind=0;
	REP1(i, N) {
		if (CanPut(r,c,i)) {
			count++;
			ind = i;
		}
	}
	if (count==1) return ind;
	return 0;
}

//trying to find out whether these arrays are used outside intended space
void CheckForProblem() {
	REP(i, MAXN) REP(j, MAXN) {
		if (i>8 || j>9) {
			if (row[i][j] || col[i][j] || blck[i][j]) OK();
		}
		if (i>8 || j>8) {
			if (grid[i][j]>0) OK();
		}
	}
}

bool Solve(int d);
bool CheckForSolution(int r, int c, int val, int d) {
	InsertCell(r,c,val);
	if (Solve(d)) return true;
	RemoveCell(r,c,val);
}

bool GridOK() {	
	bool ok;
	REP(i, N) REP(j, N) {
		if (grid[i][j]==0) {
			ok = false;
			REP1(k, N) {
				if (CanPut(i,j,k)) {
					ok = true;
					break;
				}
			}
			if (!ok) return false;
		}
	}
	return true;
}

bool Solve(int d) {
	CheckForProblem();
	if (d==N*N) { //d is depth of DFS
		return true;
	}
	if (!GridOK()) {
		return false;
	}
	
	int m, r, c;
	REP(i, N) REP(j, N) {
		if (grid[i][j]==0) {
			m = CheckCell(i,j);
			r = i;
			c = j;
			if (m!=0) {
				return CheckForSolution(i,j,m,d+1);
			}
		}
	}
	
	REP1(i, N) {
		if (CanPut(r,c,i)) {
			if (CheckForSolution(r,c,i,d+1)) {
				return true;
			}
		}
	}
	return false;
}

//main()============================================================================================================================

int main()
{
	Init();
	PRINT();
	time_t t1 = clock();
	Solve(filled);
	time_t t2 = clock();
	PRINT();
	printf("\nSolved in %.3f seconds.\n\n", (float)(t2-t1)/1000000);
}

//Function Definitions ===============================================================================================================


void Init() {
	FILE* fin = fopen("Game_Sudoku_data.txt", "r");
	int k;
	REP(i, N) REP(j, N) {
		fscanf(fin, "%d", &k);
		if (k>0) {
			InsertCell(i, j, k);
			filled++; //to start with appropriate depth
		}
	}
}

void PRINT() {
	char str[] = "  ";
	REP(i, N) {
		REP(j, N) {
			if (grid[i][j]==0)
				str[0] = ' ';
			else
				str[0] = grid[i][j] + '0';
			if ((i/3+j/3)%2)
				printf("%s", str);
			else
				printf("\033[40m%s\033[0m", str);
		}
		printf("\n");
	}
	printf("\n");
}
/*
void PRINT() {
	REP(i, N) {
		if ((i+1)%n==0 && i<N-1) printf("__");
		else printf("  ");
		REP(j, N) {
			if (j%n==0 && j>0) printf("|");
			else printf(" ");
			if (grid[i][j]>0) printf("%d", grid[i][j]);
			else printf(" ");
		}
		if ((i+1)%n==0 && i<N-1) printf("__");
		else printf("  ");
		printf("\n");
	}
	printf("\n");
}*/
