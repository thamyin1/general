#include <iostream>
#include <cstdio>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <cmath>
#include <algorithm>
using namespace std;

#define REP(i, n) for(int (i)=0; (i)<(n); (i)++)

struct Seg {
	int l, r;
	Seg () {}
	Seg(int _l, int _r) {
		l = _l;
		r = _r;
	}
};

class MyComp {
	public:
	bool operator() (Seg a, Seg b) {
		return a.l<b.l;
	}
};

int main() {
	set<int> ss;
	set<int>::iterator it;
	REP(i, 10) ss.insert(2*i+1);
	for(it=ss.begin(); it!=ss.end(); it++) {
		printf("%d ", *it);
	}
	printf("\n");
	it = ss.upper_bound(20);
	if (it!=ss.begin()) it--;
	printf("it: %d\n", *it);
	
	set<Seg, MyComp> s;
	set<Seg, MyComp>::iterator m;
	REP(i, 10) s.insert(Seg(2*i+1, -i));
	for(m=s.begin(); m!=s.end(); m++) {
		printf("%d %d\n", m->l, m->r);
	}
	m = s.upper_bound(Seg(100000, 1000));
	if (m!=s.begin()) m--;
	printf("m:: %d %d\n", m->l, m->r);
	int t;
	printf("OKOOK\n");
	cin >> t;
}
