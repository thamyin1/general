<!DOCTYPE HTML>
<html>
<head>
	<title>Sudoku Solver!</title>
	<link rel='stylesheet' type='text/css' href='stylesheet.css'/>
	<script src='http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script>
	<script type='text/javascript' src='/SudokuSolver/java.js'></script>
</head>
<body>
	<?php
		$n = 3;
		for($i=0; $i<$n; $i++) {
			for($j=0; $j<$n; $j++) {
				echo "<div class='boxes'>";
				for($k=0; $k<$n; $k++) {
					for($l=0; $l<$n; $l++) {
						$x = $n*$i+$k;
						$y = $n*$j+$l;
						echo "<input type=text class='inputfield' id='cell",$x,$y,"'>";
					}
					echo "<br/>";
				}
				echo "</div>";
			}
			echo "<br/>";
		}
	?>
	<br/>
	<button id='submit'>Solve me!</button>
	<br/>
	<div id='dump'></div>
</body>
</html>
