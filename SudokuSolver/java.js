var N = 9, n = 3, filled = 0;

var grid = new Array(N);
var row = new Array(N);
var col = new Array(N);
var blck = new Array(N);

$(document).ready(function(){
	$('button#submit').click(function(){
		Init();
		GetValues();
		if (Solve(filled)) document.write("SOLVED!!<br/>");
		else document.write("Fail!<br/>");
		for(var i=0; i<N; i++) {
			for(var j=0; j<N; j++) {
				document.write(grid[i][j]+" ");
			}
			document.write("<br/>");
		}
	});
});

function PRINTGRID() {
	for(var i=0; i<N; i++) {
		for(var j=0; j<N; j++) {
			if (grid[i][j]==0) document.write("0 ");
			else document.write(grid[i][j]+" ");
		}
		document.write("<br/>");
	}
	document.write("<br/>");
}

function PRINT(arr) {
	for(var i=0; i<N; i++) {
		for(var j=1; j<=N; j++) {
			document.write(arr[i][j]);
		}
		document.write("<br/>");
	}
	document.write("<br/>");
}

function f(r,c) {
	return n*Math.floor(r/n)+Math.floor(c/n);
}

function Init() {
	for(var i=0; i<N; i++) {
		grid[i] = new Array(N);
		row[i] = new Array(N+1);
		col[i] = new Array(N+1);
		blck[i] = new Array(N+1);
		for(var j=0; j<N; j++) {
			grid[i][j] = row[i][j+1] = col[i][j+1] = blck[i][j+1] = 0;
		}
	}
}

function GetValues() {
	var str = "input#cell";
	for(var i=0; i<N; i++) {
		for(var j=0; j<N; j++) {
			grid[i][j] = $(str+i+j).val();
			if (grid[i][j]!=0) {
				filled++;
				InsertCell(i,j,grid[i][j]);
			}
		}
	}
}

function InsertCell(r, c, k) {
	grid[r][c] = k;
	row[r][k] = col[c][k] = blck[f(r,c)][k] = 1;
}

function RemoveCell(r, c, k) {
	grid[r][c] = 0;
	row[r][k] = col[c][k] = blck[f(r,c)][k] = 0;
}

function CheckCell(r,c) {
	var count=0, ind=0;
	for(var i=1; i<=N; i++) {
		if (row[r][i]==0 && col[c][i]==0 && blck[f(r,c)][i]==0) {
			count++;
			ind = i;
		}
	}
	if (count==1) return ind;
	return 0;
}

function CheckForSolution(r,c,val,d) {
	InsertCell(r,c,val);
	if (Solve(d)) return true;
	RemoveCell(r,c,val);
}

function GridOK() {
	for(var i=0; i<N; i++) {
		for(var j=0; j<N; j++) {
			if (grid[i][j]==0) {
				var ok = false;
				for(var k=1; k<=N; k++) {	
					if (row[i][k]==0 && col[j][k]==0 && blck[f(i,j)][k]==0) {
						ok = true;
						break;
					}
				}
				if (!ok) return false;
			}
		}
	}
	return true;
}


function Solve(d) {
	if (d==N*N) return true;
	if (!GridOK()) return false;
	
	var rr=0, cc=0, m;
	for(var i=0; i<N; i++) {
		for(var j=0; j<N; j++) {
			if (grid[i][j]==0) {
				m = CheckCell(i,j);
				rr = i;
				cc = j;
				if (m!=0) {
					return CheckForSolution(i,j,m,d+1);
				}
			}
		}
	}
	for(var i=1; i<=N; i++) {
		if (row[rr][i]==0 && col[cc][i]==0 && blck[f(rr,cc)][i]==0) {
			if (CheckForSolution(rr,cc,i,d+1)) return true;
		}
	}
	return false;
}
